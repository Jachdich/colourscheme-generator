#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include <stdio.h>
#include <stddef.h>
#include <math.h>
#include <string.h>
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

float max3(float a, float b, float c) {
    return ((a > b)? (a > c ? a : c) : (b > c ? b : c));
}

float min3(float a, float b, float c) {
    return ((a < b)? (a < c ? a : c) : (b < c ? b : c));
}

struct hsv {
    float h, s, v;
};

struct rgb {
    unsigned char r, g, b;
};

struct hsv_with_weight {
    struct hsv colour;
    int weight;
};

struct hsv rgb_to_hsv(float r, float g, float b) {
    float h, s, v;
    r /= 255.0;
    g /= 255.0;
    b /= 255.0;
    float cmax = max3(r, g, b);
    float cmin = min3(r, g, b);
    float dnocapf = cmax-cmin;
    if (cmax == cmin)
        h = 0;
    else if (cmax == r)
        h = fmod((60 * ((g - b) / dnocapf) + 360), 360.0);
    else if (cmax == g)
        h = fmod((60 * ((b - r) / dnocapf) + 120), 360.0);
    else if (cmax == b)
        h = fmod((60 * ((r - g) / dnocapf) + 240), 360.0);

    if (cmax == 0)
        s = 0;
    else
        s = (dnocapf / cmax) * 100;
    v = cmax * 100;
    return (struct hsv) {
        h, s, v
    };
}

struct rgb hsv_to_rgb(float H, float S,float V) {

    float s = S/100;
    float v = V/100;
    float C = s * v;
    float X = C * (1 -fabs(fmod(H/60.0, 2) - 1));
    float m = v-C;
    float r,g,b;
    if (H >= 0 && H < 60) {
        r = C,g = X,b = 0;
    } else if (H >= 60 && H < 120) {
        r = X,g = C,b = 0;
    } else if (H >= 120 && H < 180) {
        r = 0,g = C,b = X;
    } else if (H >= 180 && H < 240) {
        r = 0,g = X,b = C;
    } else if (H >= 240 && H < 300) {
        r = X, g = 0, b = C;
    } else {
        r = C,g = 0,b = X;
    }
    int R = (r+m) * 255;
    int G = (g+m) * 255;
    int B = (b+m) * 255;
    return (struct rgb) {
        R, G, B
    };
}

int atoi_s(char *a) {
    int out = 0;
    while(* a != 0) {
        if (* a >= '0' && * a <= '9') {
            out *= 10;
            out += * a - '0';
            a++;
        } else {
            return -1;
        }
    }
    return out;
}

void write_file(int num, struct rgb r) {
    char path[256];
    sprintf(path, "/home/james/.config/wallpapers/colour/%d", num);
    FILE *fp = fopen(path, "w");
    if (fp == 0) {
        fprintf(stderr, "Error opening file. plz fix\n");
        return;
    }
    fprintf(fp, "#%02x%02x%02x\n", r.r, r.g, r.b);
    fclose(fp);
}

int mod(int a, int b) {int ret = a%b; return ret>=0? ret: ret+b; }

int main(int argc, char * * argv) {
    if (argc < 2 || argc > 4) {
        printf("Usage: %s <filename> [steps=36]\n", argv[0]);
        return 1;
    }

    int steps = 36;
    if (argc > 2) {
        int num = atoi_s(argv[2]);
        if (num == -1) {
            printf("Expected suseger value, got '%s' for steps argument", argv[2]);
            return 1;
        }
        steps = num;
    }

    int visual_out = 1;
    if (argc == 4) {
        if (strncmp(argv[3], "--script", 9) == 0) {
            visual_out = 0;
        }
    }

    int x,y,n;
    unsigned char *data = stbi_load(argv[1], &x, &y, &n, 0);
    if (data == 0) {
        printf("Error loading file '%s'\n", argv[1]);
        return 1;
    }

    //create `steps` num of hue buckets, each containing a max of
    //x * y hues (so in theory, the whole image could be the exact
    //same colour and it would not crash)
    struct hsv *hues[steps];
    int lens[steps];
    memset(lens, 0, sizeof(lens));
    for (int i = 0; i < steps; i++) {
        hues[i] = malloc(x * y * sizeof(*hues[i]));
    }

    //put pixels in appropriate bucket depending on hue
    //and only if value/sat is high enough (don't count dark colours
    //or colours too close to white)
    for (int i = 0; i < x * y; i++) {
        int idx = i * n;
        int r = data[idx];
        int g = data[idx + 1];
        int b = data[idx + 2];
        struct hsv hsv_val = rgb_to_hsv(r, g, b);
        if (hsv_val.v > 30 && hsv_val.s > 40) {
            int h = hsv_val.h / (360.0 / steps);
            hues[h][lens[h]++] = hsv_val;
        }
    }

    //find max num of hues in a hue bucket
    int max_len = 0;
    for (int i = 0; i < steps; i++) {
        if (lens[i] > max_len) {
            max_len = lens[i];
        }
    }

    //average each hue bucket, and if it has more than zero elements
    //then put the average in a new array containing only colours that exist
    struct hsv_with_weight averages[steps];
    int averages_pos = 0;
    for (int i = 0; i < steps; i++) {
        struct hsv avg = {0.0f, 0.0f, 0.0f};
        if (lens[i] > max_len / 60) {
            //calculate average
            for (int j = 0; j < lens[i]; j++) {
                avg.h += hues[i][j].h * (1.0f / lens[i]);
                avg.s += hues[i][j].s * (1.0f / lens[i]);
                avg.v += hues[i][j].v * (1.0f / lens[i]);
            }
            //add weight to average for future calc
            averages[averages_pos++] = (struct hsv_with_weight) {avg, lens[i]};
            //print colorued bar graph
            if (visual_out) {
                struct rgb r = hsv_to_rgb(avg.h, avg.s, avg.v);

                char spaces[21];
                int len_norm = (float)lens[i] / max_len * 19 + 2;
                int k;
                for (k = 0; k < len_norm; k++) {
                    spaces[k] = ' ';
                }
                spaces[k] = 0;
                printf("%02d: %02x%02x%02x:\033[48;2;%d;%d;%dm%s\033[0m\n", averages_pos - 1, r.r, r.g, r.b, r.r, r.g, r.b, spaces);
            }
        }
    }
    for (int i = 0; i < steps; i++)
        free(hues[i]);


    //calculate index in averages array of most common colour
    int max_weight = 0;
    int max_idx = 0;
    for (int i = 0; i < averages_pos; i++) {
        if (averages[i].weight > max_weight) {
            max_weight = averages[i].weight;
            max_idx = i;
        }
    }

    //get the most common averaged colour
    struct hsv h = averages[max_idx].colour;
    struct rgb r = hsv_to_rgb(h.h, h.s, 80);
    struct rgb r2 = hsv_to_rgb(h.h, h.s, h.v);
    if (visual_out) {
        //print it with defined hue/sat and without
        printf("\n\nDominant colour:\n");
        printf("%02x%02x%02x:\033[48;2;%d;%d;%dm       \033[0m\n", r.r, r.g, r.b, r.r, r.g, r.b);
        printf("%02x%02x%02x:\033[48;2;%d;%d;%dm       \033[0m\n", r2.r, r2.g, r2.b, r2.r, r2.g, r2.b);
    } else {
        //print just the colour code
        printf("#%02x%02x%02x\n", r.r, r.g, r.b);
        //now sample the range of existing colours, need 7 colours for the status bar
        int num_samples = 7;
        double step = (double)averages_pos / (double)num_samples;
        if (step < 1) step = 1;
        int file_idx = 0;
        //diff from max_idx to use
        int n = averages_pos / 2;
        fprintf(stderr, "step: %lf, n: %d, max_idx: %d, averages_pos: %d\n", step, n, max_idx, averages_pos);
        for (double i = 0; i < MAX(averages_pos, num_samples); i += step) {
            int idx = mod(floor(i), averages_pos);
            fprintf(stderr, "%d\n", idx);
            struct hsv curr_hsv = averages[idx].colour;
            if (curr_hsv.v < 70) curr_hsv.v = 80;
            struct rgb curr_rgb = hsv_to_rgb(curr_hsv.h, curr_hsv.s, curr_hsv.v);
            write_file(file_idx, curr_rgb);
            file_idx++;
        }
    }

    stbi_image_free(data);
    return 0;
}
